import { browser, by, element, protractor } from 'protractor';

export class AppPage {
  /*
  Navigates to specified url.
  */
  navigateTo(url: string = '/') {
    return browser.get(url);
  }

  /*
  Wait that page is loaded. Timeout is 5 seconds.
  */
  waitPageToLoad(page: string) {
    browser.wait(protractor.ExpectedConditions.urlContains(page), 5000);
  }

  /*
  Executes click for specified element.
  */
  clickElement(selector: string) {
    return new Promise((resolve) => {
      element(by.css(selector)).click().then(() => {
        resolve();
      });
    });
  }
  /*
  Get current page url.
  */
  getCurrentUrl() {
    return new Promise((resolve) => {
      browser.getCurrentUrl().then(data => {
        resolve(data);
      });
    });
  }

  /*
  Returns content (innerText) from element.
  */
  getTextFromElement(selector: string) {
    return element(by.deepCss(selector)).getText();
  }

  /*
  Returns value (attribute) from element.
  */
  getValueFromElement(selector: string) {
    return element(by.deepCss(selector)).getAttribute('value');
  }

   /*
  Sets input value by clicking element and sending keystrokes. Backspace is pressed once
  before typing content.
  */
  setInputValue(selector: string, value: string) {
    element(by.deepCss(selector)).click().then(() => { 
      browser.actions().sendKeys(protractor.Key.BACK_SPACE).perform().then (() => {
        browser.actions().sendKeys(value).perform();
      });
    })
  }
}
