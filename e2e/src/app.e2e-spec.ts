import { AppPage } from './app.po';
import { browser, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });
  it('should calculate limits', () => {
    page.navigateTo(); // Navigate to home.
    page.waitPageToLoad('home'); // Wait that home is loaded.
    page.setInputValue('ion-input', '30'); // Write 30 as age.
    page.clickElement('ion-button').then(() => { // Click calculate button.
      expect(page.getTextFromElement('p')).toContain('124 - 162'); // Verify result on result page.
    });
  });

  it ('should reset home page', () => {
    page.navigateTo(); // Navigate to home page.
    page.waitPageToLoad('home'); // Wait that home is loaded.
    page.setInputValue('ion-input', '30'); // Write 30 as age.
    page.clickElement('ion-button').then(() => { // Click calculate button.
      page.waitPageToLoad('result'); // Wait that result is loaded.
      page.getCurrentUrl().then (url => { // Get current url.
        expect(url).toContain('result'); // Verify that url is result.
        page.navigateTo(); // Navigate back to home.
        page.waitPageToLoad('home'); // Wat that home is loaded.
        expect(page.getValueFromElement('ion-input')).toContain('0'); // Verify that is is 0 (is reset).
      });
    });
  });

  it('should navigate back to home from result',() => {
    page.navigateTo(); // Navigate to home.
    page.waitPageToLoad('home'); // Wait that home is loaded.
    page.setInputValue('ion-input', '30'); // Write 30 as age.
    page.clickElement('ion-button').then(() => { // Click calculate button.
      page.waitPageToLoad('result'); // Wait that result is loaded.
      page.clickElement('ion-back-button').then(() => { // Click back button on navigation bar.
        page.waitPageToLoad('home'); // Wait that home is loaded.
        page.getCurrentUrl().then (url => { // Get current url.
          expect(url).toContain('home'); // Verify that url is home.
        });
      });
    });
  });
});
