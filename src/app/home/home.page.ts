import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  age: number;

  constructor(public router: Router) {
    this.age = 0;
  }

  ionViewDidEnter() {
    this.age = 0;
  }

  calculate() {
    this.router.navigateByUrl('result/' + this.age.toString());
  }
}
